<?php
/**
 * Dynamic css
*/
if (! function_exists('blogger_buzz_dynamic_css')){
    function blogger_buzz_dynamic_css(){

        $title_font = get_theme_mod('blogger_buzz_site_title_font_family','arizonia');  
        $font_size  = get_theme_mod('blogger_buzz_site_title_font_size',80);
        $px = 'px';

        $header_text_color = get_theme_mod('blogger_buzz_primary_color');

        $blogger_buzz_dynamic = '';

        // Title Fonts Typography.
        $blogger_buzz_dynamic .= "
        .bz_main_nav .site-title a{
            font-family: $title_font, cursive;
        }
        .bz_main_header .site-branding h1 a{
            font-family: $title_font, cursive;
            font-size: $font_size$px;
            
        }\n";

        if( $header_text_color){
            $blogger_buzz_dynamic .= "
                a,
                a:hover,
                a:focus,
                a:active,
                .main-navigation ul li:hover>a,
                .main-navigation ul li.current-menu-item>a,
                .main-navigation ul li.active>a,
                .main-navigation div>ul>li.menu-item-has-children:hover:before,
                .main-navigation ul ul li.menu-item-has-children:hover:before,
                .main-navigation div>ul>li.current-menu-item:before,
                .main-navigation div>ul>li.current-menu-ancestor:before,
                .header_layout_three .bz_top_header ul li.current-menu-ancestor:before,
                .header_layout_three .bz_top_header ul li.current-menu-item:before,
                .main-navigation ul li.current-menu-item:before,
                .main-navigation ul li:hover>a,
                .main-navigation ul li.current-menu-item>a,
                .main-navigation ul li.current-menu-ancestor>a,
                .header_layout_three .bz_top_header ul li.current-menu-ancestor>a,
                .header_layout_three .bz_top_header ul li.current-menu-item>a,
                .main-navigation ul li.active>a,
                .post-navigation .nav-previous a:hover,
                .posts-navigation .nav-previous a:hover,
                .post-navigation .nav-previous a:hover:before,
                .posts-navigation .nav-previous a:hover:before,
                .post-navigation .nav-next a:hover,
                .posts-navigation .nav-next a:hover,
                .post-navigation .nav-next a:hover:before,
                .posts-navigation .nav-next a:hover:before,
                .pagination .nav-links a:hover,
                .pagination .page-numbers.current,
                .widget-area .widget a:hover,
                .widget-area .widget a:hover::before,
                .widget-area .widget li:hover::before,
                .cat_toggle,
                .aboutus-wrap .sociallink ul li a,
                .widget-area .product_list_widget .woocommerce-Price-amount,

                .widget.yith-woocompare-widget .compare:hover,
                .widget.yith-woocompare-widget .clear-all:hover,
                .widget .social-link ul li a:hover,
                .widget .social-link ul li a,
                .custom-recent-post li .post-date,
                .header_layout_three .bz_main_nav .main-navigation ul li a:hover,
                .header_layout_three .bz_main_nav .main-navigation ul li.menu-item-has-children:hover:before,
                .main-navigation div>ul>li.current-menu-ancestor:before,
                .header_layout_three .bz_top_header .main-navigation div>ul>li.current-menu-ancestor:before,
                .header_layout_three .bz_top_header .main-navigation div>ul>li.current-menu-item:before,
                .header_layout_four .main-navigation ul li:hover>a,
                .header_layout_four .main-navigation ul li ul li:hover>a,
                .header_layout_four .main-navigation div>ul>li.menu-item-has-children:hover:before,
                .header_layout_four .main-navigation ul ul li.menu-item-has-children:hover:before,
                .banner-title a:hover,
                .banner_overlay ul li a:hover,
                .transparent_button,
                .transparent_button:hover,
                .bz_feature ul.meta-catagory li a,
                .bz_feature .post-title a:hover,
                .blog-style ul.meta-catagory li a,
                .blog-style .post-meta li.author-meta a,
                .blog-style .post-meta li a:hover,
                .blog-style .blog-title a:hover,
                .blog-style .social-link ul li a,
                .blog-style .social-link ul li a:hover,
                .blog-style.blog-style-two .more-link a:before,
                .breadcrumb ul li,
                .comments-area .fn,
                .reply a,
                .site-footer .widget a:hover,
                .site-footer .widget a:hover::before,
                .site-footer .widget li:hover::before,
                .site-footer .custom-recent-post li .post-date,
                .lower_bottom .copyright a,
                .lower_bottom .copyright a.privacy-policy-link:hover,
                .woocommerce-account .woocommerce-MyAccount-navigation ul li.is-active a,
                .woocommerce-account .woocommerce-MyAccount-navigation ul li:hover a,
                .woocommerce-info:before,
                .woocommerce-message:before,
                .woocommerce-MyAccount-content a:hover,
                .woocommerce-MyAccount-content a:hover,
                .woocommerce #respond input#submit.alt:hover,
                .woocommerce a.button.alt:hover,
                .woocommerce button.button.alt:hover,
                .woocommerce input.button.alt:hover,

                .woocommerce #respond input#submit:hover,
                .woocommerce a.button:hover,
                .woocommerce button.button:hover,
                .woocommerce input.button:hover,
                .woocommerce-error:before,
                .woocommerce nav.woocommerce-pagination ul li .page-numbers,
                .woocommerce ul.products li.product .price


                {
                    color : {$header_text_color};
                }

                .cross-sells h2:before,
                .cart_totals h2:before,
                .up-sells h2:before,
                .related h2:before,
                .woocommerce-billing-fields h3:before,
                .woocommerce-shipping-fields h3:before,
                .woocommerce-additional-fields h3:before,
                #order_review_heading:before,
                .woocommerce-order-details h2:before,
                .woocommerce-column--billing-address h2:before,
                .woocommerce-column--shipping-address h2:before,
                .woocommerce-Address-title h3:before,
                .woocommerce-MyAccount-content h3:before,
                .wishlist-title h2:before,
                .woocommerce-account .woocommerce h2:before,
                .widget-area .widget .widget-title:before,
                .widget h2:before,
                .wp-block-search__label:before,
                .pagination .page-numbers,
                .aboutus-wrap .sociallink ul li a,
                .aboutus-wrap .sociallink ul li a:hover,
                .widget.yith-woocompare-widget .compare,
                .widget.yith-woocompare-widget .clear-all,
                .widget.yith-woocompare-widget .compare:hover,
                .widget.yith-woocompare-widget .clear-all:hover,
                .widget .social-link ul li a,
                .transparent_button,
                .blog-style .more-link a,
                #respond .form-submit .submit,
                .wpcf7 input[type=\"submit\"],
                .site-footer .widget h2.widget-title:before,
                .woocommerce-account .woocommerce-MyAccount-navigation ul li.is-active a,
                .woocommerce-account .woocommerce-MyAccount-navigation ul li:hover a,
                .woocommerce-account .woocommerce-MyAccount-navigation ul li a,
                .woocommerce-info,
                .woocommerce-message,
                .post-detail-container,
                .woocommerce-account .woocommerce-MyAccount-content,
                .woocommerce-Message--info a.button,
                .woocommerce #respond input#submit,
                .woocommerce a.button,
                .woocommerce button.button,
                .woocommerce input.button,
                .woocommerce-cart .wc-proceed-to-checkout a.checkout-button,
                .woocommerce div.product .woocommerce-tabs ul.tabs:before,
                .woocommerce-error,
                .woocommerce nav.woocommerce-pagination ul li,
                .blog-style .social-link ul li a,
                .pagination .page-numbers

                {
                    border-color : {$header_text_color};
                }


                .widget_product_search a.button,
                .widget_product_search button,
                .widget_product_search input[type=\"submit\"],
                .widget_search .search-submit,
                .calendar_wrap caption,
                .aboutus-wrap .sociallink ul li a:hover,
                .woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content,
                .woocommerce .widget_shopping_cart .cart_list li a.remove:hover,
                .woocommerce.widget_shopping_cart .cart_list li a.remove:hover ,
                .widget.yith-woocompare-widget .compare,
                .widget.yith-woocompare-widget .clear-all,
                .bz_slider .owl-carousel .owl-nav button.owl-prev,
                .bz_slider .owl-carousel .owl-nav button.owl-next,
                .blog-style .more-link a:hover,
                #respond .form-submit .submit,
                
                .wpcf7 input[type=\"submit\"],
                .woocommerce-account .woocommerce-MyAccount-navigation ul li a,
                .woocommerce-Message--info a.button,
                .woocommerce #respond input#submit,
                .woocommerce a.button,
                .woocommerce button.button,
                .woocommerce input.button,
                .woocommerce-cart .wc-proceed-to-checkout a.checkout-button,
                .woocommerce #respond input#submit.alt,
                .woocommerce a.button.alt,
                .woocommerce button.button.alt,
                .woocommerce input.button.alt ,

                .woocommerce div.product .woocommerce-tabs ul.tabs li:hover,
                .woocommerce div.product .woocommerce-tabs ul.tabs li.active,
                .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
                .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
                .woocommerce nav.woocommerce-pagination ul li a:focus,
                .woocommerce nav.woocommerce-pagination ul li a:hover,
                .woocommerce nav.woocommerce-pagination ul li span.current,
                .pagination .page-numbers
                {
                    background-color : {$header_text_color};
                }
            ";
        }

        wp_add_inline_style( 'blogger-buzz-style', $blogger_buzz_dynamic );
    }
}
add_action( 'wp_enqueue_scripts', 'blogger_buzz_dynamic_css', 99 );