<div class="welcome-getting-started">
    <div class="welcome-demo-import">
        <h3><?php echo esc_html__('Manual Setup', 'blogger-buzz'); ?></h3>

        <h3><?php echo esc_html__('Step 1 - Manage Home Page Posts Layout', 'blogger-buzz'); ?></h3>
        <ol>
            <li><?php echo esc_html__('Go to Appearance >> Customize >> Home / Category Posts Settings >> HomePage Posts Settings', 'blogger-buzz'); ?></li>
            <li><?php echo esc_html__('Select Home Blog Posts Layout Per as you want.', 'blogger-buzz'); ?></li>
            <li><?php echo esc_html__('Save changes', 'blogger-buzz'); ?></li>
        </ol>
        <div class="theme-steps">
			<img src="<?php echo esc_url(get_template_directory_uri() .'/welcome/css/homepage-blog-posts.gif'); ?>" alt="<?php echo esc_attr__('Select Home Page Blog Posts Layout', 'blogger-buzz'); ?>">
		</div>

        <p><strong><?php echo esc_html__('FROM ELEMENTOR', 'blogger-buzz'); ?></strong></p>
        <ol>
            <li><?php echo esc_html__('Firstly install and activate "Elementor Website Builder" plugin from', 'blogger-buzz'); ?> <a href="<?php echo esc_url(admin_url('post-new.php?page=bloggerbuzz-welcome&section=recommended_plugins')); ?>" target="_blank"><?php echo esc_html__('Recommended Plugin page.', 'blogger-buzz'); ?></a></li>
            <li><?php echo esc_html__('Create a new page and edit with Elementor. Drag and drop the elements in the Elementor to create your own design.', 'blogger-buzz'); ?></li>
            <li><?php echo esc_html__('Now go to Appearance &gt; Customize &gt; Homepage Settings and choose "A static page" for "Your latest posts" and select the created page for "Home Page" option.', 'blogger-buzz'); ?></li>
        </ol>
        
        <p>
            <?php echo sprintf(esc_html__('For more detail visit our theme %s.', 'blogger-buzz'), '<a href="'.esc_url('https://docs.sparklewpthemes.com/bloggerbuzz/').'" target="_blank">' . esc_html__('Documentation Page', 'blogger-buzz') . '</a>'); ?>
        </p>
    </div>

    <div class="welcome-demo-import">
        <h3><?php echo esc_html__('Demo Importer', 'blogger-buzz'); ?><a href="<?php echo esc_url('https://sparklewpthemes.com/wordpress-themes/free-ecommerce-wordpress-theme/'); ?>" target="_blank" class="button button-primary"><?php esc_html_e('Download Demo Data', 'blogger-buzz'); ?></a></h3>
        <div class="welcome-theme-thumb">
            <img src="<?php echo esc_url(get_stylesheet_directory_uri() . '/screenshot.png'); ?>" alt="<?php printf(esc_attr__('%s Demo', 'blogger-buzz'), $this->theme_name); ?>">
        </div>

        <div class="welcome-demo-import-text">
            <p><?php esc_html_e('Or you can get started by importing the demo with just one click.', 'blogger-buzz'); ?></p>
            <p><?php echo sprintf(esc_html__('Click on the button below to install and activate the One Click Demo Importer Plugin. For more detailed documentation on how the demo importer works, click %s.', 'blogger-buzz'), '<a href="'.esc_url('https://docs.sparklewpthemes.com/bloggerbuzz/').'" target="_blank">' . esc_html__('here', 'blogger-buzz') . '</a>'); ?></p>
            <?php echo $this->generate_demo_installer_button(); ?>
        </div>
    </div>
</div>